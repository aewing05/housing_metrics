from clients.acs_api import CensusAPIClient
from datetime import datetime
from enum import Enum

ACS5 = 'acs5'
ACS5_YEAR = 2021
MICHIGAN_CODE = '26'

# EMPLOYMENT STATUS FOR THE POPULATION 16 YEARS AND OVER
class EmploymentFields(Enum):
    EMPLOY_STATUS_TOTAL = 'B23025_001E'
    EMPLOY_STATUS_TOTAL_IN_LABOR_FORCE = 'B23025_002E'
    EMPLOY_STATUS_TOTAL_IN_LABOR_FORCE_CIVILIAN = 'B23025_003E'
    EMPLOY_STATUS_TOTAL_IN_LABOR_FORCE_CIVILIAN_EMPLOYED = 'B23025_004E'
    EMPLOY_STATUS_TOTAL_IN_LABOR_FORCE_CIVILIAN_UNEMPLOYED = 'B23025_005E'
    EMPLOY_STATUS_TOTAL_IN_LABOR_FORCE_MILITARY = 'B23025_006E'
    EMPLOY_STATUS_TOTAL_NOT_IN_LABOR_FORCE = 'B23025_007E'

data_codes = [field.value for field in EmploymentFields]

client = CensusAPIClient(ACS5, ACS5_YEAR, MICHIGAN_CODE, data_codes)
data = client.get_data()

state_id = []
state_name = []
county_id = []
county_name = []
tract = []
block_group = []
census_tract_fips_code = []

for index, row in data.iterrows():
    state_code = row['geo'].geo[0][1]
    county_code = row['geo'].geo[1][1]
    tract_code = row['geo'].geo[2][1]

    state_id.append(state_code)
    county_id.append(county_code)
    tract.append(tract_code)
    block_group.append(row['geo'].geo[3][1])
    # Concatenation of the 2-digit state code, the 3-digit county code, and the 6-digit tract code.
    census_tract_fips_code.append(state_code + county_code + tract_code)

    name_data = row['geo'].name.split(', ')
    state_name.append(name_data[3])
    county_name.append(name_data[2])

data['state_id'] = state_id
data['state_name'] = state_name
data['county_id'] = county_id
data['county_name'] = county_name
data['tract'] = tract
data['block_group'] = block_group
data['census_tract_fips_code'] = census_tract_fips_code


updated_data = data.rename(columns = {field.value: field.name for field in EmploymentFields})


now = datetime.now() # current date and time
date_str = now.strftime("%m%d%Y")
updated_data.to_csv('~/src/acs_five_year_mi_data' + '_' + date_str + '.csv')