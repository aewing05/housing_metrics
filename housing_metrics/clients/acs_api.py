# from fredapi import Fred
import censusdata


class CensusAPIClient:

    def __init__(self, data, year, state_code, data_codes):
        self.data = data
        self.year = year
        self.state_code = state_code
        self.data_codes = data_codes

        self._censusgeo = censusdata.censusgeo(
            [('state', state_code),('county', '*'),('block group', '*')]
        )

    def get_data(self):
        data = censusdata.download(self.data, self.year, self._censusgeo, self.data_codes)
        data.reset_index(inplace=True)
        return data.rename(columns = {'index':'geo'})
        
