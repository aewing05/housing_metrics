from fredapi import Fred

class FredAPIClient:

    def __init__(self, api_key=None):
        self._api_key = api_key

        if api_key is None:
            raise ValueError('You must supply an API key to the FredAPIClient')

        self._fred_conn = Fred(api_key=self._api_key)

    def get_data(self, series_id):
        return self._fred_conn.get_series(series_id)
        
