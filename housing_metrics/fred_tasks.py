import yaml
from clients.fred_api import FredAPIClient

credentials = None
# TODO: Better file path management
with open("../credentials.yml", "r") as stream:
    try:
        credentials = yaml.safe_load(stream)
        print(credentials)
    except yaml.YAMLError as exc:
        print(exc)
    finally:
        stream.close()

fred_apikey = credentials['fredapi']['apikey']

mortgage_30_us_series_id = 'MORTGAGE30US'
fred_client = FredAPIClient(fred_apikey)
mortgage_30_us_data = fred_client.get_data(mortgage_30_us_series_id)
print(mortgage_30_us_data)
