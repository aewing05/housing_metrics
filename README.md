# Overview

# Developer Guide
This was run in a virtualenv on Python 3.11.2. Create one and install packages from requirements.txt
` pip install -r requirements.txt`

Copy the credentials seed file, and add your API keys. The credentials.yml is in .gitignore to prevent actual credentials from entering the repo. 
`cp credentials.yml.seed credentials.yml`
